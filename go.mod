module gitlab.com/tezoscommons/tezostats

go 1.17

require (
	github.com/Jeffail/gabs/v2 v2.6.1 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
)
