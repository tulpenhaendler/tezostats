package main

import "fmt"
import "encoding/json"
import "os/signal"
import "os"



func main() {
	s := NewStats()

	// group by month
	s.Groupfunc = func (in *parsed) string {
		return in.when.Format("2006-01")
	}

	// print results
	res := s.GetResults()
	go func(){
		for {
			r := <- res
			b,_ := json.Marshal(r)
			fmt.Println(string(b))
		}
	}()

	// get blocks
	// 1200000
	blockchan := parse(fetch("https://indexers.tcinfra.net/rpc/mainnet", 1200000))
	go func() {
		for {
		i := <- blockchan
		s.Ingest(i)
		}
	}()




	// keep running until interrupt
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<- c
}


