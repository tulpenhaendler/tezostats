package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/hashicorp/golang-lru"
)

type internalStat struct {
	unique bool
	val    string
	name   string
}

type Stats struct {
	Groupfunc func(in *parsed) string
	outchans  []chan Result

	lastbucket string
	buckets    map[string][]internalStat

	cache *lru.Cache
}

type Result struct {
	Bucket string
	Name   string
	Value  uint64
}

func (s *Stats) GetResults() chan Result {
	out := make(chan Result)
	s.outchans = append(s.outchans, out)
	s.buckets = map[string][]internalStat{}
	s.lastbucket = "begin"
	return out
}

func (s *Stats) submit(res Result) {
	for _, o := range s.outchans {
		o <- res
	}
}

func NewStats() *Stats {
	s := Stats{}
	s.outchans = []chan Result{}
	s.cache, _ = lru.New(10000)
	return &s
}

func (s *Stats) count(bucket, name, val string) {
	s.is(bucket, name, val, true)
}

func (s *Stats) is(bucket, name, val string, unique bool) {
	if val == "null" {
		return
	}
	if s.lastbucket == "" {
		s.lastbucket = bucket
	} else {
		if s.lastbucket != bucket {
			// aggregate old stats
			res := map[string]Result{}
			uniqs := map[string]bool{}

			for _, x := range s.buckets[s.lastbucket] {
				st := x
				if _, ok := res[st.name]; !ok {
					res[st.name] = Result{
						Value:  0,
						Name:   st.name,
						Bucket: s.lastbucket,
					}
				}

				if st.unique == false {
					a := res[st.name]
					a.Value++
					res[st.name] = a
				} else {
					key := st.name + st.val
					if _, ok := uniqs[key]; !ok {
						a := res[st.name]
						a.Value++
						res[st.name] = a
						uniqs[key] = true
					}
				}
			}

			for _, r := range res {
				s.submit(r)
			}

			// delete old tmp stats
			s.buckets = map[string][]internalStat{}
			s.lastbucket = bucket
		}
	}
	a := internalStat{
		unique: unique,
		name:   name,
		val:    val,
	}
	if _, ok := s.buckets[bucket]; !ok {
		s.buckets[bucket] = []internalStat{}
	}
	s.buckets[bucket] = append(s.buckets[bucket], a)
}

func (s *Stats) increment(bucket, name string) {
	s.is(bucket, name, "1", false)
}

func (s *Stats) Ingest(parsed *parsed) {
	group := s.Groupfunc(parsed)

	// create NFT operations
	cnftop := [][]string{
		{"KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton", "mint"},        // 	HEN
		{"KT1Aq4wWmVanpQhq4TTfjZXB5AjFpx15iQMM", "mint_artist"}, // 	Objkt
		{"KT1EpGgjQs73QfFJs9z7m1Mxm5MTnpC2tqse", "mint"},        // 	Kalamint
		{"KT1LjmAdYQCLBjwv4S2oFkEzyHVkomAf5MrW", "mint"},        // 	Versum
		{"KT1AEVuykWeuuFX7QkEAMNtffzwhe1Z98hJS", "mint_issuer"}, // 	fxHash
		{"KT1XCoGnfupWk7Sp8536EfrxcP73LmT68Nyr", "mint_issuer"}, // 	fxHash
		{"KT1DdxmJujm3u2ZNkYwV24qLBJ6iR7sc58B9", "mint"},        // Tezzard crowdsale
		{"KT1PWsARJzTfYWithkr2NTmcc9pwsZFi9JC2", "mint"},        // Mekatron crowdsale
		{"KT1B7o9UQArZGrfedHhYqG7VTWDTtkVop2X2", "mint"},        // Neonz crowdsale
	}

	// defi contracts
	defikt := []string{
		"KT1WBLrLE2vG8SedBqiSJFm4VVAZZBytJYHc", // Quipu
		"KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5", // LB
		"KT1PwoZxyv4XkPEGnTqWYvjA1UYiPTgAGyqL", // Spicyswap
		"KT1HaDP8fRW7kavK2beST7o4RvzuvZbn5VwV", // Plenty
		"KT1KnuE87q1EKjPozJ5sRAjQA24FPsP57CE3", // Crunchy
		"KT1MfMMsYX34Q9cEaPtk4qkQ6pojA7D2nsgr", // Plenty
	}

	for _, oo := range parsed.operations {
		for _, o := range oo.S("contents").Children() {

			// count active wallets
			sender := o.S("source").String()
			s.count(group, "active_wallets", sender)

			dest := o.S("destination").String()
			dest = strings.Trim(dest, "\"")
			if strings.HasPrefix(dest, "KT") {
				//number of contract calls
				s.increment(group, "contract_calls")

				// number of active contracts
				s.count(group, "active_contracts", dest)

				// nft mints
				entrypoint := o.Path("parameters.entrypoint").String()
				entrypoint = strings.Trim(entrypoint, "\"")
				entrypoint = strings.ToLower(entrypoint)
				for _, info := range cnftop {
					if dest == info[0] && entrypoint == info[1] {
						s.increment(group, "nft_minted")

						// number of different artists minting
						s.count(group, "nft_artists", sender)
					}
				}

				// unique wallets interacting with defi
				for _, addr := range defikt {
					if dest == addr {
						s.count(group, "defi_wallets", sender)
					}
				}

				// active fa1.2 tokens
				// check if contract has fa1.2 interface
				if val, ok := s.cache.Get(dest); ok {
					if val.(bool) == true {
						s.count(group, "active_fa12_tokens", dest)
					}
				} else {
					// get entrypoints
					res, err := http.Get("https://api.tzkt.io/v1/contracts/" + dest + "/entrypoints")
					if err == nil {
						b, _ := ioutil.ReadAll(res.Body)
						epmodel := []Entrypoints{}
						json.Unmarshal(b, &epmodel)
						istoken := 0
						must := []string{"transfer","approve","getAllowance","getBalance", "getTotalSupply"}
						for _,m := range must {
							for _,e := range epmodel {
								if m == e.Name {
									istoken++
								}
							}
						}
						if istoken >= 4 {
							s.cache.Add(dest, true)
							s.count(group, "active_fa12_tokens", dest)
						} else {
							s.cache.Add(dest, false)
						}
					}
				}

			}
		}

	}
}

type Entrypoints struct {
	Name           string `json:"name"`
}
