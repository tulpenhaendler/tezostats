package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/Jeffail/gabs/v2"
)

const cmbh = "/chains/main/blocks/"

type parsed struct {
	level   string
	when    time.Time
	content map[string]interface{}
	parsed *gabs.Container
	operations []*gabs.Container
}

type opstmp struct {
	Operations [][]json.RawMessage
}

func parse(in chan []byte) chan *parsed {
	out := make(chan *parsed)

	go func() {
		for {
			res := parsed{}
			raw := <-in
			jsonParsed, _ := gabs.ParseJSON(raw)
			res.parsed = jsonParsed
			level := jsonParsed.Path("header.level").String()
			res.level = strings.Trim(level, "\"")
			ts := jsonParsed.Path("header.timestamp").String()
			res.when,_ = time.Parse("2006-01-02T15:04:05Z", strings.Trim(ts, "\""))

			t := opstmp{}
			json.Unmarshal(raw, &t)
			ops := []*gabs.Container{}
			for _,a := range t.Operations {
				for _,b := range a {
					op,_ := gabs.ParseJSON(b)
					ops = append(ops, op)
				}
			}
			res.operations = ops
			out <- &res
		}
	}()

	return out

}

type fetchres struct {
	wg       *sync.WaitGroup
	res      []byte
	err      error
	client   *http.Client
	endpoint string
}

func (f *fetchres) get(i int) {
	tries := 0
	var err error
	for {
		if tries > 5 {
			break
		}
		tries = tries + 1
		is := strconv.Itoa(i)
		res, err := f.client.Get(f.endpoint + cmbh + is)
		if err != nil {
			time.Sleep(1 * time.Second)
		} else {
			f.res, _ = ioutil.ReadAll(res.Body)
			res.Body.Close()
			break
		}
	}
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	f.wg.Done()
}

func fetch(endpoint string, start int) chan []byte {
	res := make(chan []byte)
	pending := make(chan *fetchres, 300)
	client := http.Client{}

	go func() {
		for {
			next := fetchres{
				wg:       &sync.WaitGroup{},
				res:      nil,
				err:      nil,
				client:   &client,
				endpoint: endpoint,
			}
			next.wg.Add(1)
			i := start
			go next.get(i)
			start = start + 1
			pending <- &next
		}
	}()

	go func() {
		for {
			next := <-pending
			next.wg.Wait()
			res <- next.res
		}
	}()

	return res
}
