package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"strconv"
)


type Row struct {
	Name string
	Value int
	Bucket string
}


func main(){
	table := map[string]map[string]int{}
	headers := []string{}

	f,_ := os.Open("./results.txt")

	rows := []Row{}
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		b := scanner.Bytes()
		r := Row{}
		json.Unmarshal(b, &r)
		rows = append(rows, r)
	}

	for _,r := range rows {
		if _,ok := table[r.Bucket]; !ok {
			table[r.Bucket] = map[string]int{}
		}
		table[r.Bucket][r.Name] = r.Value
		headers = append(headers, r.Name)
	}


	fmt.Print("Bucket;")
	h := removeDuplicateStr(headers)
	for _,s := range h {
		fmt.Print(s + ";")
	}
	fmt.Println("Value;")

	for k,v := range table {
		fmt.Print(k + ";")
		for _,s := range h {
			if _,ok := v[s]; ok {
				fmt.Print(strconv.Itoa(v[s]) + ";")
			} else {
				fmt.Print(";")
			}
		}
		fmt.Print("\n")
	}

}


func removeDuplicateStr(strSlice []string) []string {
	allKeys := make(map[string]bool)
	list := []string{}
	for _, item := range strSlice {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
			list = append(list, item)
		}
	}
	return list
}
